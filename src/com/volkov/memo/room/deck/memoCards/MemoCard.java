package com.volkov.memo.room.deck.memoCards;


import com.volkov.memo.room.deck.Card;
import javafx.scene.image.Image;

public class MemoCard extends Card {
    public MemoCard(int number) {
        super(number);
        faceImage = new Image(getClass().getResourceAsStream("images/" + number + ".jpg"));

    }

    @Override
    public void saveTheCoordinates(int xCoord, int yCoord) {
        super.oX = xCoord;
        super.oY = yCoord;
        super.coordinates = new int[]{xCoord, yCoord};
    }


}
