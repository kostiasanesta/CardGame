package com.volkov.memo.room.deck;


import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public abstract class Card {
    protected int[] coordinates;
    protected int oX;
    protected int oY;
    private int cardNumber;
    private Image shirtImg;
    protected Image faceImage;
    private ImageView img;
    private final static String imageSource = "images/shirt.jpg";
    public static final int IMAGE_HEIGHT = 110;
    public static final int IMAGE_WIDTH = 70;

    public Card(int number) {
        cardNumber = number;
        shirtImg = new Image(getClass().getResourceAsStream(imageSource));
        img = new ImageView(shirtImg);
        img.setFitHeight(IMAGE_HEIGHT);
        img.setFitWidth(IMAGE_WIDTH);

    }

    public abstract void saveTheCoordinates(int xCoord, int yCoord);

    public ImageView getImg() {
        return img;
    }


    public Image getFaceImage() {
        return faceImage;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public Image getShirtImg() {
        return shirtImg;
    }

    public int[] getCoordinates() {
        return coordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Card card = (Card) o;

        return cardNumber == card.cardNumber;
    }

    @Override
    public int hashCode() {
        return cardNumber;
    }

    public int getoX() {
        return oX;
    }

    public int getoY() {
        return oY;
    }
}
