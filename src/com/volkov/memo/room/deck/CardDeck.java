package com.volkov.memo.room.deck;


import java.util.ArrayList;
import java.util.List;

public class CardDeck {
    private List<Card> deck;


    public static final int DECK_CAPACITY = 50;

    public CardDeck() {
        deck = new ArrayList<>(DECK_CAPACITY);
    }

    public List<Card> getDeck() {
        return deck;
    }
}
