package com.volkov.memo.room.table.computer;


import com.volkov.memo.room.table.ComputerPlayer;

public class HardComputer extends ComputerPlayer {
    public HardComputer(String name) {
        super(name);
    }

    @Override
    public void toRemember() {
        switch ((int) (Math.random() * 5)) {
            case 1:
                break;
            case 2:
                processOfRemembering();
                break;
            case 3:
                processOfRemembering();
                break;
            case 4:
                processOfRemembering();
                break;
            default:
                break;
        }
    }
}
