package com.volkov.memo.room.table;


import com.volkov.memo.room.deck.Card;


import java.util.*;


public abstract class ComputerPlayer extends Player {
    protected Map<int[], Card> rememberedCards;
    protected Set<Card> set;

    public ComputerPlayer(String name) {
        super(name);
        rememberedCards = new HashMap<>();
    }


    public abstract void toRemember();

    protected void processOfRemembering(){
        if (!rememberedCards.containsKey(rememberedCard.getCoordinates())) {
            rememberedCards.put(rememberedCard.getCoordinates(), rememberedCard);
        }
    }


    public Card computerTurn(List<Card> deck, int counter) {
        if (counter == 0) {
            rememberedCard = null;
            set = new HashSet<>();
            int size;
            for (Iterator<int[]> it = rememberedCards.keySet().iterator(); it.hasNext(); ) {
                int[] key = it.next();
                Card card = rememberedCards.get(key);
                if (!deck.contains(card)) {
                    rememberedCards.remove(card);
                } else {
                    size = set.size();
                    set.add(card);
                    if (size == set.size()) {
                        rememberedCard = card;
                    }
                }
            }
            if (rememberedCard == null) {
                rememberedCard = deck.get((int) (Math.random() * deck.size()));
            }
        } else {
            Card finded = null;
            for (Card need : set) {
                if (need.equals(rememberedCard) && (!(need.getCoordinates().equals(rememberedCard.getCoordinates())))) {
                    finded = need;
                }
            }
            if ((finded == null) || (finded.getoX() == rememberedCard.getoX() && finded.getoY() == rememberedCard.getoY())) {
                rememberedCard = deck.get((int) (Math.random() * deck.size()));
            }else{
                rememberedCard = finded;
            }
        }
        rememberedCard.getImg().setImage(rememberedCard.getFaceImage());
        return rememberedCard;
    }


}
