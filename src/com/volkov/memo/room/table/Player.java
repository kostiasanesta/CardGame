package com.volkov.memo.room.table;


import com.volkov.memo.room.deck.Card;


public abstract class Player {
    private String name;
    private int score;
    protected int strokeCounter;
    protected Card rememberedCard;
    private int turnNumber;

    public String getName() {
        return name;
    }

    public Player(String name) {
        this.name = name;
        score = 0;
        turnNumber = 0;
    }

    public int getTurnNumber() {
        return turnNumber;
    }

    public void setTurnNumber(int turnNumber) {
        this.turnNumber = turnNumber;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    public void setRememberedCard(Card e) {
        this.rememberedCard = e;
    }
}
