package com.volkov.memo.room;


import com.volkov.memo.gui.Table;
import com.volkov.memo.room.deck.Card;
import com.volkov.memo.room.deck.CardDeck;
import com.volkov.memo.room.deck.memoCards.MemoCard;
import com.volkov.memo.room.table.ComputerPlayer;
import com.volkov.memo.room.table.ManPlayer;
import com.volkov.memo.room.table.Player;
import com.volkov.memo.room.table.computer.EasyComputer;
import com.volkov.memo.room.table.computer.HardComputer;
import com.volkov.memo.room.table.computer.MiddleComputer;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


//Delller, person who control the game.

public class Dealler {
    private CardDeck cardDeck;
    private ComputerPlayer computerPlayer;
    private Player player;
    private Card firstCard;
    private Card secondCard;
    private Table table;
    private int counter;
    private boolean canRemove;
    private boolean humanTurn;
    private GridPane decomposedDeck;
    private Thread computerThread;
    private boolean buttonValue;
    private boolean removedCardsValue;


    public Dealler(int modeChanger) {
        cardDeck = new CardDeck();
        player = new ManPlayer("Player");
        if (modeChanger == 1) {
            computerPlayer = new EasyComputer("Baby");
        } else if (modeChanger == 2) {
            computerPlayer = new MiddleComputer("CowBoy");
        } else {
            computerPlayer = new HardComputer("BATIANIA");
        }

    }


    public GridPane putOnTable() {
        decomposedDeck = new GridPane();
        int oY = 1;
        int oX = 1;
        for (Card e : cardDeck.getDeck()) {
            if (!(oX <= Table.OX_TABLE_LAYOUT)) {
                oX = 1;
                oY++;
            }
            e.saveTheCoordinates(oX, oY);
            decomposedDeck.add(e.getImg(), oX, oY);
            oX++;
        }
        oY++;
        Table.setFinalBordersOfCards(oX, oY);
        return decomposedDeck;
    }


    public void createDeck() {
        int i = 1;
        while (cardDeck.getDeck().size() < CardDeck.DECK_CAPACITY) {
            cardDeck.getDeck().add(new MemoCard(i));
            cardDeck.getDeck().add(new MemoCard(i));
            i++;
        }
    }


    public void kneadDeck() {
        for (int i = 0; i < 1000; i++) {
            int x = (int) (Math.random() * 50);
            int y = (int) (Math.random() * 50);
            firstCard = cardDeck.getDeck().get(x);
            secondCard = cardDeck.getDeck().get(y);
            cardDeck.getDeck().set(y, firstCard);
            cardDeck.getDeck().set(x, secondCard);
        }

    }


    public void runTheGame(Stage stage) {
        createDeck();//create deck of card
        kneadDeck();//"tysyet" this deck
        table = new Table();//create game table with our kneaded deck
        table.setDecomposedDeck(putOnTable());
        chooseWhoGoFirst();
//        table.addTheButton(humanTurn);
//        table.setVisibleButton(true);
        showScore();//tell about the gamers score
        try {
            table.start(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        createTheRuleOnThisGame();//create some rules
        preporationForNextTurn(!humanTurn);
    }

    private void decompositTable() {
        if (canRemove) {
            removedCardsValue = true;
            cardDeck.getDeck().remove(firstCard);
            cardDeck.getDeck().remove(secondCard);
            if (humanTurn) {
                player.setScore(player.getScore() + 1);
            } else {
                computerPlayer.setScore(computerPlayer.getScore() + 1);
            }
        } else {
            if (humanTurn) {
                humanTurn = false;
            } else {
                humanTurn = true;
            }
            firstCard.getImg().setImage(firstCard.getShirtImg());
            secondCard.getImg().setImage(secondCard.getShirtImg());
            removedCardsValue = false;
        }
        decomposedDeck = new GridPane();
        for (Card e : cardDeck.getDeck()) {
            decomposedDeck.add(e.getImg(), e.getoX(), e.getoY());
        }
        showScore();
        table.setDecomposedDeck(decomposedDeck);
    }

    public void showScore() {
        table.createComputerLabel(computerPlayer.getName(), computerPlayer.getScore());
        table.createPlayerLabel(player.getName(), player.getScore());
    }

    private void playGame(int increment, Card openedCard) {
        counter += increment;
        if (counter == 1) {
            firstCard = openedCard;
        }
        if (counter == 2) {
            secondCard = openedCard;
            if (firstCard.getCardNumber() == openedCard.getCardNumber()) {
                canRemove = true;
            } else {
                canRemove = false;
            }
        }

    }

    private void createThePlayerRuleOnThisGame() {
        for (Card e : cardDeck.getDeck()) {
            e.getImg().setOnMouseClicked(event -> {
                System.out.println("X=" + e.getoX() + " y=" + e.getoY());
                System.out.println("card number = " + e.getCardNumber());
                if (counter < 2 && humanTurn) {
                    e.getImg().setImage(e.getFaceImage());
                    computerPlayer.setRememberedCard(e);
                    computerPlayer.toRemember();
                    playGame(1, e);
                    System.out.println(counter);
                }
                if (counter == 2) {
                    buttonValue = true;
                    preporationForNextTurn(humanTurn);
                    System.out.println(counter);
                }
            });
        }
    }

    private void banCardPress() {
        for (Card e : cardDeck.getDeck()) {
            e.getImg().setOnMouseClicked(event -> {

            });
        }
    }


    private void addButtonRule() {
        table.getNextTurnButton().setOnMouseClicked(event -> {

            if (counter < 2 && (!humanTurn)) {
                preporationForNextTurn(!humanTurn);
                computersTurn();
            } else if (counter == 2 && (!humanTurn)) {
                decompositTable();
                checkToContiniusTurn();
                preporationForNextTurn(!humanTurn);
                counter = 0;
            } else if (counter == 2) {
                counter = 0;
                decompositTable();
                checkToContiniusTurn();
                preporationForNextTurn(!humanTurn);
            }
        });
    }

    private void computersTurn() {
        computerThread = new Thread(new compTurner());
        computerThread.setDaemon(true);
        computerThread.start();
    }

    private void checkToContiniusTurn() {
        if (removedCardsValue && (!humanTurn)) {
            buttonValue = true;
        } else if (removedCardsValue && humanTurn) {
            buttonValue = false;
        } else if ((!removedCardsValue) && (!humanTurn)) {
            buttonValue = true;
        } else if ((!removedCardsValue) && humanTurn) {
            buttonValue = false;
        }

    }

    private void preporationForNextTurn(boolean visibleValue) {
        table.addTheButton(humanTurn);
        table.setVisibleButton(visibleValue);
        table.nextTurn();
        if (buttonValue) {
            banCardPress();
            addButtonRule();
        } else {
            createThePlayerRuleOnThisGame();
        }
    }

    private void chooseWhoGoFirst() {
        switch ((int) (Math.random() * 2)) {
            case 1:
                humanTurn = true;
                buttonValue = false;
                break;
            default:
                humanTurn = false;
                buttonValue = true;
                break;
        }

    }

    private class compTurner implements Runnable {
        @Override
        public void run() {
            int i = 0;
            while (i < 2) {
                playGame(1, computerPlayer.computerTurn(cardDeck.getDeck(), counter));
                computerPlayer.toRemember();
                try {
                    Thread.currentThread().sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i++;
            }

            computerThread.stop();
        }


    }
}
