package com.volkov.memo;


import com.volkov.memo.gui.Menu;




public class MemoAdministrator{
    Menu menu;

    public MemoAdministrator() {
        menu = new Menu();
    }

    public static void main(String[] args) {
        MemoAdministrator app = new MemoAdministrator();
        app.runTheApp();
    }


    private void runTheApp() {
        Thread thread = new Thread( new Menu());
        thread.start();
    }

}