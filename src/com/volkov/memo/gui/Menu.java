package com.volkov.memo.gui;


import com.volkov.memo.room.Dealler;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Menu extends Application implements Runnable {
    private Scene scene;
    private Button play;
    private Stage stage;


    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = new BorderPane();
        root.setPadding(new Insets(10, 10, 10, 10));
        VBox toggleBox = new VBox(5);

        ToggleButton easyBtn = new ToggleButton("Baby");
        ToggleButton midleBtn = new ToggleButton("Cowboy");
        ToggleButton hardBtn = new ToggleButton("Batyanya!");

        ToggleGroup groupOfHard = new ToggleGroup();
        easyBtn.setToggleGroup(groupOfHard);
        midleBtn.setToggleGroup(groupOfHard);
        hardBtn.setToggleGroup(groupOfHard);

        easyBtn.setUserData(1);
        midleBtn.setUserData(2);
        hardBtn.setUserData(3);
        midleBtn.setSelected(true);

        toggleBox.getChildren().addAll(easyBtn, midleBtn, hardBtn);
        root.setTop(toggleBox);
        play = new Button("Start");
        root.setCenter(play);
        play.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                runTheGame((int) groupOfHard.getSelectedToggle().getUserData());
            }
        });

        scene = new Scene(root, 1200, 700);
        primaryStage.setTitle("Memo");
        this.stage = primaryStage;
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void runTheGame(int userData) {
       Dealler dealler = new Dealler(userData);
        try {
            stage.close();
            dealler.runTheGame(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        launch();

    }
}
