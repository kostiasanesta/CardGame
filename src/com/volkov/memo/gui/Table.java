package com.volkov.memo.gui;


import com.volkov.memo.gui.borders.HBorder;
import com.volkov.memo.gui.borders.VBorder;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;


public class Table extends Application {
    private Stage stage;
    private Scene gameScene;
    public static final int OX_TABLE_LAYOUT = 10;
    private GridPane decomposedDeck;
    private BorderPane borderPane;
    private Label playerNameLabel;
    private Label computerNameLabel;
    private Label playerScoreLabel;
    private Label computerScoreLabel;
    private VBox leftVbox;
    private VBox rightVbox;
    private static int oX;
    private static int oY;
    private Button nextTurnButton;

    public Table() {

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        createBorderPane();
        gameScene = new Scene(borderPane, 1250, 700);
        primaryStage.setScene(gameScene);
        primaryStage.show();

    }

    public void createBorderPane() {
        borderPane = new BorderPane();
        addBorders();
        borderPane.setBottom(nextTurnButton);
        borderPane.setCenter(decomposedDeck);
        borderPane.setRight(rightVbox);
        borderPane.setLeft(leftVbox);
        BorderPane.setMargin(nextTurnButton, new Insets(-125, 500, 150, 570));
        BorderPane.setMargin(decomposedDeck, new Insets(0, 100, 100, 100));
        BorderPane.setMargin(rightVbox, new Insets(300, 5, 300, -60));
        BorderPane.setMargin(leftVbox, new Insets(300, 70, 300, 25));
    }

    public void nextTurn() {
        try {
            start(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createComputerLabel(String name, int score) {
        rightVbox = new VBox(20);
        computerNameLabel = new Label(name);
        computerScoreLabel = new Label(String.valueOf(score));
        computerNameLabel.setTextFill(Color.RED);
        computerScoreLabel.setTextFill(Color.RED);
        computerNameLabel.setFont(new Font(30));
        computerScoreLabel.setFont(new Font(30));
        rightVbox.getChildren().addAll(computerNameLabel, computerScoreLabel);


    }

    public void addTheButton(boolean playerType) {
        nextTurnButton = new Button();
        nextTurnButton.setPrefSize(150, 75);
        nextTurnButton.setFont(new Font(15));
        if (playerType) {
            nextTurnButton.setText("End your turn!");
            nextTurnButton.setTextFill(Color.YELLOW);
            nextTurnButton.setStyle("-fx-background-color: blue");
        } else {
            nextTurnButton.setText("Computer turn!");
            nextTurnButton.setTextFill(Color.BLACK);
            nextTurnButton.setStyle("-fx-background-color: pink");
        }
    }

    public void setVisibleButton(boolean visibleValue) {
        nextTurnButton.setVisible(visibleValue);
    }

    public void createPlayerLabel(String name, int score) {
        leftVbox = new VBox(20);
        playerNameLabel = new Label(name);
        playerScoreLabel = new Label(String.valueOf(score));
        playerNameLabel.setTextFill(Color.BLUE);
        playerScoreLabel.setTextFill(Color.BLUE);
        playerNameLabel.setFont(new Font(30));
        playerScoreLabel.setFont(new Font(30));
        leftVbox.getChildren().addAll(playerNameLabel, playerScoreLabel);

    }


    public void setDecomposedDeck(GridPane decomposedDeck) {
        this.decomposedDeck = decomposedDeck;
    }

    public void addBorders() {

        int x = 1;
        for (int y = 0; x < oX; x++) {
            decomposedDeck.add(new HBorder().getBorder(), x, y);
            decomposedDeck.add(new HBorder().getBorder(), x, oY);
        }
        int y = 1;
        for (x = 0; y < oY; y++) {
            decomposedDeck.add(new VBorder().getBorder(), x, y);
            decomposedDeck.add(new VBorder().getBorder(), oX, y);
        }

    }

    public static void setFinalBordersOfCards(int lastX, int lastY) {
        oX = lastX;
        oY = lastY;
    }

    public Button getNextTurnButton() {
        return nextTurnButton;
    }
}
