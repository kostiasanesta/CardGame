package com.volkov.memo.gui;


import javafx.scene.image.ImageView;

public class Border {
    protected static final String IMG_SOURCE = "images/";
    protected static final String V_IMAGE_NAME = "V.jpg";
    protected static final String H_IMAGE_NAME = "H.jpg";
    private ImageView border;
    public static final int DEPTH = 10;

    public Border(){
        border = new ImageView();
    }

    public ImageView getBorder() {
        return border;
    }

}
