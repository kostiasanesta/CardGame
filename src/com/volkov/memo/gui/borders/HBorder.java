package com.volkov.memo.gui.borders;


import com.volkov.memo.gui.Border;
import com.volkov.memo.room.deck.Card;
import javafx.scene.image.Image;

public class HBorder extends Border {
    public HBorder() {
        super();
        getBorder().setImage(new Image(getClass().getResourceAsStream(IMG_SOURCE + H_IMAGE_NAME)));
        getBorder().setFitHeight(DEPTH);
        getBorder().setFitWidth(Card.IMAGE_WIDTH);
    }
}
