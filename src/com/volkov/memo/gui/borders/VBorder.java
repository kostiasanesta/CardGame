package com.volkov.memo.gui.borders;


import com.volkov.memo.gui.Border;
import com.volkov.memo.room.deck.Card;
import javafx.scene.image.Image;

public class VBorder extends Border {
    public VBorder() {
        super();
        getBorder().setImage(new Image(getClass().getResourceAsStream(IMG_SOURCE + V_IMAGE_NAME)));
        getBorder().setFitWidth(DEPTH);
        getBorder().setFitHeight(Card.IMAGE_HEIGHT);
    }
}
